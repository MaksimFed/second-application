package com.example.secondApp;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class AppController {

    @GetMapping("/hello")
    public ResponseEntity<?> getHelloMessage(){
        log.info("#GET /hello");
        return ResponseEntity.ok("Hello from second app!");
    }
}
